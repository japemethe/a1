/// <reference types="cypress" />

describe('Test Web', () => {
  it('test one', () => {
    // throw error in test
    //throw new Error('Oopss...');

    cy.visit('https://codedamn.com'); // visiting codedamn.com
    cy.contains('Programming'); // is site contain string above?
    cy.contains('Programming').should('exist'); // is site contain string above?

    cy.get('div#root'); // get div element
    cy.get('div[id=root]').should('exist'); // is div id root exist

    cy.contains('Learn Programming').should('have.text', 'Learn Programming'); // is site contain string above?

    cy.viewport('iphone-5') // change viewport
  });

  // * Disable comment if you want to run multiple test
//  it('test one', () => {
//    cy.visit('https://codedamn.com'); // visiting codedamn.com
//    
//  });
});

describe('Basic test', () => {
  it('Reload the browser then change viewport', () => {
    cy.reload(); // it will reload the broweser
    cy.viewport(128, 720)
  });
});